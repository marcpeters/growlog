# Developing

- To watch SASS files for changes and compile, run: ```sass --watch ./sass/:./client/css/```
- To start the server, run: ```node app.js```
-- To start the server so that it auto-reloads on change, run: ```nodemon app.js```.  This requires that `nodemon` is installed.  To install it, enter ```sudo npm install -g nodemon```
- To start mongodb, run: ```sudo mongod```