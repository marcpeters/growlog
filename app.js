var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var pg = require('pg');
var connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/growlog';
var app = express();

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var util = require('util');

var count = 0;

app.use(express.static('client'));
app.use('/js/angular', express.static(__dirname + '/node_modules/angular'));
app.use('/js/angular-route', express.static(__dirname + '/node_modules/angular-route'));
app.use('/js/angular-resource', express.static(__dirname + '/node_modules/angular-resource'));

app.set('json spaces', 2);

// app.get('/', function (req, res) {
//   res.send('Hello World!');
// });

app.listen(3000, function () {
  console.log('Grow Log server listening on port 3000');
});

app.post('/createdbschema', function(req, res) {
	var client = new pg.Client(connectionString);
	client.connect();
	var query = client.query("CREATE TABLE IF NOT EXISTS Vendor(id SERIAL PRIMARY KEY, name TEXT, websiteUrl TEXT)");
	var query2 = client.query("CREATE TABLE IF NOT EXISTS Subtype(id SERIAL PRIMARY KEY, name TEXT)");
	var query3 = client.query("CREATE TABLE IF NOT EXISTS Species(id SERIAL PRIMARY KEY, name TEXT)");
	var query4 = client.query("CREATE TABLE IF NOT EXISTS Plant(id SERIAL PRIMARY KEY, given_id TEXT, species references Species(id), subtype references Subtype(id), vendor references Vendor(id))");
	query3.on('end', function() { 
		client.end(); 
		res.set("Content-Type", "application/json");
	    res.set(200);
	    res.set("Connection", "close");
	    res.send({success: true});
	    res.end();
	});
});

app.post('/plant/add', jsonParser, function(req, res) {

	MongoClient.connect("mongodb://localhost:27017/growlog", function(err, db) {
		if(err) {
			console.log(err);
		}
		else {
			console.log("Connected to mongodb");

			var collection = db.collection('plants');
			req.body.recordType = 'plant';

		    // Insert some users
		    collection.insert(req.body, function (err, result) {
		      if (err) {
		        console.log(err);
		      } else {
		        console.log('Inserted %d documents into the "plants" collection: ', result.insertedCount, util.inspect(result, {showHidden: false, depth: null}));
		      }
		      //Close connection
		      db.close();
		    });
		}
	});

	res.set("Content-Type", "text/plain");
    res.set(200);
    res.set("Connection", "close");
    res.end();
});

app.get('/plants', function(req, res) {

	MongoClient.connect("mongodb://localhost:27017/growlog", function(err, db) {
		if(err) {
			console.log(err);
		}
		else {
			console.log("Connected to mongodb");

			var collection = db.collection('plants');

		    // Insert some users
		    collection.find().toArray(function (error,result) {
		      if (error) {
		        console.log(error);
		        errorResponse(res, error, "Failed to find plants");
		      } else {
		        console.log('Found these documents in the "plants" collection: ', util.inspect(result, {showHidden: false, depth: null}));
	        	res.set("Content-Type", "application/json");
			    res.set(200);
			    res.set("Connection", "close");
			    res.send({success: true, data: result});
			    res.end();
		      }
		      //Close connection
		      db.close();
		    });
		}
	});
});

app.delete('/plants', function(req, res) {
	MongoClient.connect("mongodb://localhost:27017/growlog", function(err, db) {
		if(err) {
			console.log(err);
		}
		else {
			console.log("Connected to mongodb");

			var collection = db.collection('plants');

		    // Delete all plants
		    collection.deleteMany({}, function (error,result) {
		      if (error) {
		        console.log(error);
		        errorResponse(res, error, "Failed to delete plants");
		      } else {
		        console.log('Deleted all documents in the "plants" collection: ', util.inspect(result, {showHidden: false, depth: null}));
	        	res.set("Content-Type", "application/json");
			    res.set(200);
			    res.set("Connection", "close");
			    res.send({success: true, data: result});
			    res.end();
		      }
		      //Close connection
		      db.close();
		    });
		}
	});
});

app.get('/species', function(req, res) {

	MongoClient.connect("mongodb://localhost:27017/growlog", function(err, db) {
		if(err) {
			console.log(err);
		}
		else {
			console.log("Connected to mongodb");

			var collection = db.collection('plants');

		    // Insert some users
		    collection.find({ recordType: 'species' }).toArray(function (error,result) {
		      if (error) {
		        console.log(error);
		        errorResponse(res, error, "Failed to find species");
		      } else {
		        console.log('Found these species: ', util.inspect(result, {showHidden: false, depth: null}));
	        	res.set("Content-Type", "application/json");
			    res.set(200);
			    res.set("Connection", "close");
			    res.send({success: true, data: result});
			    res.end();
		      }
		      //Close connection
		      db.close();
		    });
		}
	});
});

app.use(function(req, res) {
  res.sendFile(__dirname + '/client/index.html');
});

function errorResponse(res, error, msg) {
	res.set("Content-Type", "application/json");
    res.set(500);
    res.set("Connection", "close");
    res.send({success: false, data: null, message: msg});
    res.end();
}