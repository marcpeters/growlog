CREATE TABLE IF NOT EXISTS Vendor(
	id SERIAL PRIMARY KEY, 
	name TEXT, 
	websiteUrl TEXT);

CREATE TABLE IF NOT EXISTS Subtype(
	id SERIAL PRIMARY KEY, 
	name TEXT);

CREATE TABLE IF NOT EXISTS Species(
	id SERIAL PRIMARY KEY, 
	name TEXT);

CREATE TABLE IF NOT EXISTS Plant(
	id SERIAL PRIMARY KEY, 
	given_id TEXT, 
	species references Species(id), 
	subtype references Subtype(id), 
	vendor references Vendor(id));
