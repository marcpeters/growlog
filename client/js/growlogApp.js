"use strict";

var growlogApp = angular.module('growlogApp', [
		'ngRoute',
		'ngResource'
	]);

growlogApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider.when('/', {
		templateUrl: 'views/home.html',
		controller: 'PlantController'
	})
	.when('/plants/add', {
		templateUrl: 'views/newPlant.html',
		controller: 'PlantController'
	})
	.when('/plants/view', {
		templateUrl: 'views/viewPlants.html',
		controller: 'PlantController'
	})
	.when('/plants/delete', {
		templateUrl: 'views/deletePlants.html',
		controller: 'PlantController'
	});

	$locationProvider.html5Mode(true);
}]);