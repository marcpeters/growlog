'use strict';

var growlogApp = angular.module('growlogApp');

growlogApp.controller('PlantController', ['$scope', 'plantService', function($scope, plantService) {

	plantService.allPlants().then(function (response) {
		$scope.allPlants = response;
	});

	$scope.newPlant = {};

	$scope.allSpecies = [{name: "New...", id: "-1"}].concat([
		{name: "Strawberry", id: "1"}, 
		{name: "Basil", id: "2"}]);

	$scope.newSpeciesOption = $scope.allSpecies[0];

	$scope.newPlant.species = $scope.newSpeciesOption;

	$scope.submit = function(plant) {
		console.log("Clicked submit");
		if(plant.species == $scope.newSpeciesOption)
		{
			plantService.addPlant(plant.name, $scope.newSpeciesName);
		}
		else {
			plantService.addPlant(plant.name, plant.species.name);
		}
		$scope.submitSuccess = true;
	
	}

	$scope.deleteAllPlants = function() {
		plantService.deleteAllPlants();
	}
}]);