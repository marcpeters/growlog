'use strict';

var growlogApp = angular.module('growlogApp');

growlogApp.service('plantService', ['$http', function($http) {

	this.addPlant = function(name, species) {
		$http({
			method: 'POST',
			url: '/plant/add',
			data: {
				name: name,
				species: {
					name: species
				}
			}
		});
	};

	this.allPlants = function() {
		return $http({
			method: 'GET',
			url: '/plants'
		}).then(function (response) {
			return response.data.data;
		});
	};

	this.deleteAllPlants = function() {
		return $http({
			method: 'DELETE',
			url: '/plants'
		}).then(function (response) {
			return response.data.data;
		});
	}

	this.allSpecies = function() {
		return $http({
			method: 'GET',
			url: '/species'
		}).then(function (response) {
			return response.data.data;
		});
	}

}]);